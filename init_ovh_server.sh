#!/bin/bash

usermod -u 1001 debian
groupmod -g 1001 debian

find / -user 1000 -exec chown -h debian {} \;
find / -group 1000 -exec chgrp -h debian {} \;

adduser deployer

# Add Docker's official GPG key:
apt-get update
apt-get install -y git vim ca-certificates curl
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update

apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

groupadd docker
usermod -aG docker deployer

systemctl enable docker.service
systemctl enable containerd.service
